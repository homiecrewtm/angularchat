# OR
FROM node:10-alpine

# Create app directory
WORKDIR /usr/src/app

# Install app dependencies
# A wildcard is used to ensure both package.json AND package-lock.json are copied
# where available (npm@5+)
COPY . .

COPY package*.json ./

RUN npm install --no-optional && cd chatfront && npm i --no-optional && cd ..

# Bundle app source
COPY . .

EXPOSE 8080

CMD [ "npm", "run", "start:server:dev" ]