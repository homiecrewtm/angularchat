'use strict';
const config = require('config');
const { user, password, database, host, dialect } = config.get('db');
const Sequelize = require('sequelize');
const sequelize = new Sequelize(database, user, password, {
	host,
	dialect
});

const User = require('./user')(sequelize);
const UserRole = require('./userRole')(sequelize);

const models = {
	[User.name]: User,
	[UserRole.name]: UserRole
};
Object.keys(models).forEach((modelName) => {
	if (models[modelName].associate) {
		models[modelName].associate(models);
	}
});
module.exports = {
	sequelize,
	...models
};
