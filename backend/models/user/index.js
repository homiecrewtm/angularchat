'use strict';

const Sequelize = require('sequelize');

module.exports = (sequelize) => {
	const User = sequelize.define(
		'User',
		{
			email          : {
				type      : Sequelize.STRING(50),
				unique    : true,
				allowNull : false,
				index     : true
			},

			passwordHash   : {
				type   : Sequelize.STRING(70),
				unique : true,
				index  : true
			},
			confirmedEmail : {
				type         : Sequelize.BOOLEAN,
				allowNull    : false,
				defaultValue : false
			}
		},
		{
			underscored : false,
			tablename   : 'Users'
		}
	);

	User.associate = (models) => {
		User.hasMany(models.UserRole, { onDelete: 'cascade' });
	};
	return User;
};
