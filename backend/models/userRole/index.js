'use strict';

const Sequelize = require('sequelize');

module.exports = (sequelize) => {
	const UserRole = sequelize.define(
		'UserRole',
		{
			role : {
				type      : Sequelize.ENUM,
				allowNull : false,
				values    : [
					'user',
					'admin',
					'alphaAdmin'
				]
			}
		},
		{
			underscored : false,
			tablename   : 'UserrRoles'
		}
	);

	UserRole.associate = (models) => {
		UserRole.belongsTo(models.User, { onDelete: 'cascade' });
	};
	return UserRole;
};
