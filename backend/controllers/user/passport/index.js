'use strict';

const LocalStrategy = require('passport-local').Strategy;
const passport = require('koa-passport');

const { options, localStrategyCallback, serialize, deserializeUser } = require('./passportConfig');

passport.use('local', new LocalStrategy(options, localStrategyCallback));

passport.serializeUser(serialize);
passport.deserializeUser(deserializeUser);

module.exports = passport;
