'use strict';

const jwt = require('jsonwebtoken');
const config = require('config');
const keys = config.get('keys');

const { User } = require('../../../models');
const { compareHashes } = require('../../controllerServices');

module.exports = {
	options               : {
		usernameField : 'email',
		passwordField : 'password'
	},
	localStrategyCallback : async (email, password, done) => {
		const user = await User.findOne({
			where : {
				email
			}
		});

		if (user === null) {
			return done(null, false);
		}

		const match = await compareHashes(password, user.dataValues.passwordHash);

		if (email === user.dataValues.email && match) {
			const token = jwt.sign(user.dataValues, keys[0]);
			const { id, login, confirmedEmail, UserRoles } = user;
			const user_ = {
				id,
				login,
				confirmedEmail,
				UserRoles,
				token
			};
			return done(null, user_);
		}
		return done(null, false);
	},
	serialize             : (user, done) => {
		done(null, user.id);
	},
	deserializeUser       : async (userId, done) => {
		const user = await User.findOne({
			where : {
				id : userId
			}
		});
		if (user === null) {
			return done(null, false);
		}
		return done(null, user.toJSON());
	}
};
