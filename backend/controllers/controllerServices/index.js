const compareHashes = require('./compareHashes'),
	encrypt = require('./encrypt');

module.exports = {
	compareHashes,
	encrypt
};
