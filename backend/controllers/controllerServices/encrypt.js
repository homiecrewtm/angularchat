'use strict';

const bcrypt = require('bcrypt');
module.exports = (password) => {
	return new Promise((res, rej) => {
		bcrypt.genSalt(1024, (error, salt) => {
			if (error) rej(error.message);
			bcrypt.hash(password, salt, null, (err, result) => {
				if (err) rej(err);
				res(result);
			});
		});
	});
};
