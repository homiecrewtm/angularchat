'use strict';

const bcrypt = require('bcrypt');

module.exports = (password, hash) => {
	return new Promise((res, rej) => {
		bcrypt.compare(password, hash, (err, result) => {
			if (err) rej(err);
			res(result);
		});
	});
};
