'use sctrict';

module.exports = (socket, events, actions) => {
	if (events.constructor !== Array) {
		throw new Error('Provide events array instead');
	}
	if (actions.constructor !== Array) {
		throw new Error('Provide actions array instead');
	}
	if (events.length !== actions.length) {
		throw new Error('Actions quantity must equal events quantity');
	}
	events.forEach((event, index) => {
		socket.on(event, actions[index]);
	});
};
