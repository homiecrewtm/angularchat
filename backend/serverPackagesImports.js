'use strict';

const io = require('socket.io'),
	Koa = require('koa'),
	cors = require('koa2-cors'),
	serve = require('koa-static'),
	http = require('http'),
	config = require('config'),
	logger = require('koa-logger'),
	session = require('koa-generic-session'),
	SequelizeSessionStore = require('koa-generic-session-sequelize'),
	router = require('./routes'),
	passport = require('./controllers/user/passport'),
	sequelize = require('sequelize'),
	koaBody = require('koa-body');

module.exports = {
	io,
	koaBody,
	Koa,
	sequelize,
	config,
	cors,
	serve,
	http,
	logger,
	router,
	session,
	SequelizeSessionStore,
	passport
};
