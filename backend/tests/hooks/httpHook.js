'use strict';
const chai = require('chai');
const chaiHttp = require('chai-http');
const server = require('../../server');
chai.use(chaiHttp);

before(async function() {
	this.timeout(60000);
	this.server = await server;
});
