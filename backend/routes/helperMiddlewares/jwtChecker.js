'use strict';

const jwt = require('jsonwebtoken');
const config = require('config');

const keys = config.get('keys');

function tokenChecker(token) {
	if (token.constructor !== String) {
		throw new Error('Token must be string');
	}
	const isVerified = jwt.verify(token, keys[0]);
	return isVerified;
}

module.exports = async (ctx, next) => {
	try {
		const { token } = ctx.session.passport;

		if (!token) {
			throw new Error('Provide token');
		}

		const isVerified = tokenChecker(token);

		if (!isVerified) {
			throw new Error('Inappropriate token');
		}
		return await next();
	} catch (err) {
		return ctx.throw(401, err.message);
	}
};
