'use strict';

const Router = require('koa-router');

const { reloadMiddleware /*jwtChecker*/ } = require('./helperMiddlewares');
const loginRouter = require('./login');

const router = new Router();

router.get('*', async (ctx) => {
	return await reloadMiddleware(ctx);
});

// router.prefix('/api/v1');

router.use(loginRouter.routes()).use(loginRouter.allowedMethods());

router.get('/', (ctx) => (ctx.body = 'Hello world'));

router.get('/changs', (ctx) => {
	return (ctx.body = 'Hello');
});

module.exports = router;
