'use strict';

const packages = require('./serverPackagesImports');
const { sequelize } = require('./models');

const PORT = process.env.PORT || 8080;

async function configureApp(app) {
	await sequelize.sync();
	app.use(packages.serve('public/dist'));

	app.use(
		packages.koaBody({
			multipart  : true,
			formidable : {
				uploadDir      : './public/arenas',
				keepExtensions : true
			}
		})
	);
	// app.use(
	// packages.cors({
	// credentials : false
	// })
	// );
	app.keys = packages.config.get('keys');
	app.use(packages.logger());
	app.use(
		packages.session({
			store : new packages.SequelizeSessionStore(sequelize, {
				tableName : 'sessions'
			})
		})
	);
	app.use(packages.passport.initialize());
	app.use(packages.passport.session());
	app.use(async function(ctx, next) {
		ctx.set('Access-Control-Allow-Origin', '*');
		ctx.set('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
		ctx.set('Access-Control-Allow-Methods', 'POST, GET, PUT, DELETE, OPTIONS');
		await next();
	});
	app.use(packages.router.routes()).use(packages.router.allowedMethods());
}

async function createKoaApp() {
	const APP = new packages.Koa();
	await configureApp(APP);
	const httpServer = packages.http.createServer(APP.callback());

	return httpServer;
}

async function socketWrapper() {
	const server = await createKoaApp();
	const socketIo = packages.io(server);
	server.listen(PORT, () => {
		// eslint-disable-next-line no-console
		console.log(`listening for connections on ${PORT}`);
	});
	socketIo.on('connection', function(socket) {
		// eslint-disable-next-line no-console
		console.log(`a user ${socket.id} connected`);
		socket.on('disconnect', () => {
			// eslint-disable-next-line no-console
			console.log(`user ${socket.id} disconnected`);
		});
	});
}

async function startServer() {
	return await socketWrapper();
}

const server = startServer();

module.exports = server;
