import { Injectable } from '@angular/core';
import * as io from 'socket.io-client';
import { Observable } from 'rxjs';

@Injectable({
	providedIn: 'root'
})
export class SocketService {
	private host: string = window.location.hostname + ':' + 8080;
	private socket: any;
	constructor() {
		console.log(this.host);
		this.socket = io.connect(this.host);
		this.socket.on('connect', () => this.connected());
		this.socket.on('disconnect', () => this.disconnected());
		this.socket.on('error', (error: string) => {
			console.log(`ERROR: "${error}" (${this.host})`);
		});
	}
	connect() {
		this.socket.connect();
	}
	disconnect() {
		this.socket.disconnect();
	}
	emit(chanel: string, message: any) {
		return new Observable<any>((observer) => {
			console.log(`emit to ${chanel}:`, message);
			this.socket.emit(chanel, message, (data: any) => {
				if (data.success) {
					observer.next(data.msg);
				} else {
					observer.error(data.msg);
				}
				observer.complete();
			});
		});
	}
	on(eventName: string) {
		return new Observable<any>((observer) => {
			this.socket.off(eventName);
			this.socket.on(eventName, (data: any) => {
				observer.next(data);
			});
		});
	}
	private connected() {
		console.log('Connected');
	}
	private disconnected() {
		console.log('Disconnected');
	}
}
