import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MainChatComponent } from './components/main-chat/main-chat.component';
import { ChatRoutingModule } from './chat-routing.module';
import { SocketService } from './services/socket/socket.service';

@NgModule({
	declarations: [
		MainChatComponent
	],
	imports: [
		CommonModule,
		ChatRoutingModule
	],
	providers: [
		SocketService
	]
})
export class ChatModule {}
