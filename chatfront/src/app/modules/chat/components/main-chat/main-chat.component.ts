import { Component, OnInit } from '@angular/core';
import { SocketService } from '../../services/socket/socket.service';

@Component({
	selector: 'app-main-chat',
	templateUrl: './main-chat.component.html',
	styleUrls: [
		'./main-chat.component.sass'
	]
})
export class MainChatComponent implements OnInit {
	constructor(private socket: SocketService) {
		this.socket.connect();
	}

	ngOnInit() {
		// this.socket.emit
	}
}
