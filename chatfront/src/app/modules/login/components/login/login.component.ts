import { Component, OnInit } from '@angular/core';
import { Validators, FormControl } from '@angular/forms';

@Component({
	selector: 'app-login',
	templateUrl: './login.component.html',
	styleUrls: [
		'./login.component.scss'
	]
})
export class LoginComponent implements OnInit {
	constructor() {}

	email = new FormControl('', [
		Validators.required,
		Validators.email
	]);
	password = new FormControl('', [
		Validators.required,
		Validators.minLength(8),
		Validators.pattern(/\d+[a-zA-Z]+/)
	]);

	getErrorMessage() {
		return this.email.hasError('required')
			? 'You must enter a value'
			: this.email.hasError('email') ? 'Not a valid email' : '';
	}
	getPasswordErrorMessage() {
		return this.password.hasError('required')
			? 'You must enter a value'
			: this.email.invalid ? 'Password must be at least 8 symbols and 1 digit' : '';
	}

	ngOnInit() {}
}
