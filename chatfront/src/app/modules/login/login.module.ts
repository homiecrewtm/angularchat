import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoginComponent } from './components/login/login.component';
import { LoginRoutingModule } from './login-routing.module';
import {
	MatButtonModule,
	MatCheckboxModule,
	MatFormFieldModule,
	MatInputModule,
	MATERIAL_SANITY_CHECKS,
	MatToolbarModule,
	MatMenuModule,
	MatIconModule,
	MatCardModule,
	MatDatepickerModule,
	MatNativeDateModule,
	MatRadioModule,
	MatSelectModule,
	MatOptionModule,
	MatSlideToggleModule
} from '@angular/material';
import { CdkObserveContent, ObserversModule } from '@angular/cdk/observers';
import { ReactiveFormsModule, ControlContainer, FormsModule } from '@angular/forms';

@NgModule({
	declarations: [
		LoginComponent
	],
	imports: [
		CommonModule,
		LoginRoutingModule,
		FormsModule,
		ReactiveFormsModule,
		MatButtonModule,
		MatInputModule,
		MatFormFieldModule,
		MatButtonModule,
		MatCheckboxModule,
		MatInputModule,
		MatFormFieldModule,
		MatToolbarModule,
		MatMenuModule,
		MatIconModule,
		MatCardModule,
		MatDatepickerModule,
		MatNativeDateModule,
		MatRadioModule,
		MatSelectModule,
		MatOptionModule,
		MatSlideToggleModule
	],
	providers: []
})
export class LoginModule {}
