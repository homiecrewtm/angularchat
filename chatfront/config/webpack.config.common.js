'use strict';

const CleanWebpackPlugin = require('clean-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const autoprefixer = require('autoprefixer');

const { root } = require('./helpers');
// const isDev = process.env.NODE_ENV !== 'production';

module.exports = {
	entry   : {
		vendor    : root('src', 'vendor.ts'), //'./src/vendor.ts',
		polyfills : root('src', 'polyfills.ts'),
		main      : root('src', 'main.ts')
	},

	resolve : {
		extensions : [
			'.ts',
			'.js',
			'.json'
		]
	},

	module  : {
		rules : [
			{ test: /\.json$/, loader: 'json' },
			{
				test   : /\.jpe?g$|\.ico$|\.gif$|\.png$|\.svg$|\.woff$|\.ttf$|\.wav$|\.mp3$/,
				loader : 'file-loader?name=favicon.ico' // <-- retain original file name
			},
			{
				test   : /\.html$/,
				loader : 'html-loader'
			},
			{
				test     : /\.(scss|sass)$/,
				use      : [
					'to-string-loader',
					{ loader: 'css-loader', options: { sourceMap: true } },
					{
						loader  : 'postcss-loader',
						options : {
							plugins   : [
								autoprefixer({
									browsers : [
										'ie >= 8',
										'last 4 version'
									]
								})
							],
							sourceMap : true
						}
					},
					{ loader: 'sass-loader', options: { sourceMap: true } }
				],
				sinclude : root('src')
			}
		]
	},

	plugins : [
		new CleanWebpackPlugin(),

		new HtmlWebpackPlugin({
			// template : 'angularfrontend/src/index.html'
			template : 'src/index.html'
		})
	]
};
